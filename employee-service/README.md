# Prequal Service

### Requirements

* Python 3.7
* Pipenv

### To run in local:
  
$ `cd to  [project root directory]`

$ `pip install pipenv`

$ `pipenv --python 3.7`

$ `pipenv shell`

$ `pipenv install -e .`

$ `employee run`

#### To see routes

$ `employee routes`

#### To init tables

$ `employee init`

#### Help
$ `employee --help`

#### To deactivate environment
$ `exit`

### Microservice Server with swagger will be flashing in http://0.0.0.0:5000/api/v1/

### Quick Run - Make setup
##### To run server

$ `make init`

$ `make run`

## DB Migration

$ `CREATE DATABASE employee_service in MYSQL`

# IN PIPENV SHELL

$ `flask db init`

$ `flask db migrate`

$ `flask db upgrade`

## API ENDPOINTS

### All employees
- Path : `/employees`
- Method: `GET`
- Response: `200`

### Create employee
- Path : `/employees`
- Method: `POST`
- Fields: `empName, empDepartment, empMobile, empAge, empDesignation, empAddress`
- Response: `201`

### Details a employee
- Path : `/employees/{id}`
- Method: `GET`
- Response: `200`

### Update employee
- Path : `/employees/{id}`
- Method: `PUT`
- Fields: `empName, empDepartment, empMobile, empAge, empDesignation, empAddress`
- Response: `200`

### Delete employee
- Path : `/employees/{id}`
- Method: `DELETE`
- Response: `204`




