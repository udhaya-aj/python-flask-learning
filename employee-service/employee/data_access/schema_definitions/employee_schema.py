from marshmallow import fields

from employee.data_access.db_models import Employees
from employee.extensions import ma


class EmployeeSchema(ma.ModelSchema):

    eployees = fields.Nested('LenderSchema', only=['id', 'empName', 'empDepartment', 'empMobile', 'empAge', 'empDesignation', 'empAddress'])

    class Meta:
        model = Employees
