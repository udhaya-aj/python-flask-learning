from employee.data_access.db_models.base import Base
from employee.extensions import db


class Employees(db.Model, Base):
    """Basic Product model
    """
    id = db.Column(db.Integer, primary_key=True)
    empName = db.Column(db.String(30))
    empDepartment = db.Column(db.String(30))
    empMobile = db.Column(db.String(11))
    empAge = db.Column(db.Integer())
    empDesignation = db.Column(db.String(30))
    empAddress = db.Column(db.String(30))
    createdAt = db.Column(db.DateTime(timezone=True), default=datetime.utcnow)
    updatedAt = db.Column(db.DateTime(timezone=True), default=datetime.utcnow, onupdate=datetime.utcnow)


    def __init__(self, **kwargs):
        super(Employees, self).__init__(**kwargs)

    def __repr__(self):
        return "<Employees %s>" % self.id
