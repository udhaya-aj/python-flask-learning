import pytest
import json
import requests
url = 'http://127.0.0.1:5000' 
data= {
	"empName": "raj kumar",
	"empDepartment":"Software Developer",
	"empMobile": "988799898",
	"empAge":25,
	"empDesignation":"Senior Developer",
	"empAddress":"chennai"
}
headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
def test_get_all_employee():
    response = requests.get(url+'/api/v1/employees/')
    # json_data = json.loads(response.text)
    # print("Service is Up",response.json())
    assert response.status_code == 200

def test_get_employee():
    response = requests.get(url+'/api/v1/employees/10')
    # json_data = json.loads(response.text)
    # print("Service is Up",response.json())
    assert response.status_code == 200   

def test_create_employee():
    response = requests.post(url+'/api/v1/employees/',data=json.dumps(data),headers=headers)
    # json_data = json.loads(response.text)
    # print("Service is Up",response.json())
    assert response.status_code == 201

def test_delete_employee():
    response = requests.delete(url+'/api/v1/employees/9')
    # json_data = json.loads(response.text)
    # print("Service is Up",response.json())
    assert response.status_code == 200

def test_updata_employee():
    response = requests.put(url+'/api/v1/employees/10',data=json.dumps(data),headers=headers)
    # json_data = json.loads(response.text)
    # print("Service is Up",response.json())
    assert response.status_code == 200