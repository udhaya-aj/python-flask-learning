import os

"""Default configuration

Use env var to override
"""
DEBUG = True
SECRET_KEY = "umst"
DB_HOST = os.environ.get('DB_HOST', 'mysql:3306')
DB_USER = os.environ.get('DB_USER', 'root')
DB_PASSWORD = os.environ.get('DB_PASSWORD', 'root')
DB_PASSWORD = os.environ.get('DB_PASSWORD', 'password')
SQLALCHEMY_DATABASE_URI = "mysql://{}:{}@{}/{}".format(DB_USER, DB_PASSWORD, DB_HOST, DB_NAME)
SQLALCHEMY_TRACK_MODIFICATIONS = False
RESTPLUS_VALIDATE = True
RESTPLUS_MASK_SWAGGER = True
