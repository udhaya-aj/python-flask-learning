from flask_restplus import fields

from employee.middleware.restplus import api

employee = api.model('Employee', {
    'empName': fields.String(required=True),
    'empDepartment': fields.String(required=True),
    'empMobile': fields.String(required=True),
    'empAge': fields.Integer(required=True),
    'empDesignation': fields.String(required=True),
    'empAddress': fields.String(required=True),
})
