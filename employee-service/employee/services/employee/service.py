import collections
from datetime import datetime
from employee.extensions import db

from flask import jsonify, request
from flask_restplus import Resource
from marshmallow import fields
from sqlalchemy.orm import sessionmaker

from employee.data_access.db_models import Employees
from employee.data_access.schema_definitions.employee_schema import EmployeeSchema
from employee.extensions import db, ma
from employee.middleware.restplus import api
from employee.serializers.employee_serializer import employee


def get_employee(employee_id):
    schema = EmployeeSchema()
    employee = Employees.query.filter_by(id=employee_id).first()
    if employee != None:
        response = schema.dump(employee)
    else:
        response = {'message': "Employee not found"}
    return response


def update_employee(employee_id, employee_data):
    schema = EmployeeSchema(partial=True)
    employee = get_employee(employee_id)
    if not employee.get('message'):
        data = db.session.query(Employees).get(employee_id)
        data.empName = employee_data.get('empName') or data.empName
        data.empDepartment = employee_data.get(
            'empDepartment') or data.empDepartment
        data.updatedAt = datetime.now()
        data.empMobile = employee_data.get('empMobile') or data.empMobile
        data.empAge = employee_data.get('empAge') or data.empAge
        data.empDesignation = employee_data.get(
            'empDesignation') or data.empDesignation
        data.empAddress = employee_data.get('empAddress') or data.empAddress
        db.session.commit()
        employee_data['id'] = employee['id']
    else:
        employee_data = employee
    return employee_data


def delete_employee(employee_id):
    response = get_employee(employee_id)
    if not response.get('message'):
        employee = Employees.query.get_or_404(employee_id)
        employee.delete()
        response = None
    return response


def get_all_employee():
    schema = EmployeeSchema(many=True)
    query = Employees.query
    response = schema.dump(query)
    return response


def post_employee(data):
    employees = Employees(empName=data.get('empName'), empDepartment=data.get('empDepartment'), empMobile=data.get('empMobile'), empAge=data.get(
        'empAge'), empDesignation=data.get('empDesignation'), empAddress=data.get('empAddress'), createdAt=datetime.now(), updatedAt=datetime.now())
    response = employees.save()
    data['id'] = response.id
    return data


def bulk_insert():
    print('bulk insert')
    users_to_insert = [dict(empName="john"), dict(
        empName="mary"), dict(empName="susan")]
    db.session.bulk_insert_mappings(Employees, users_to_insert)
    db.session.commit()
    return 'Bulk insert succeed'



def bulk_update():
    user_mappings = [
        {
            'id': 1,
            'empName': 'udhaya'
        },
        {'id': 2,
         'empName': 'udhaya'
         },
        {'id': 3,
         'empName': 'udhaya'
         }]
    db.session.bulk_update_mappings(Employees, user_mappings)
    db.session.commit()
    return 'Bulk update succeed'