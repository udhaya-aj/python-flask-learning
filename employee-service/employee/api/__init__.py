from flask import Blueprint
from flask_restplus import Api, Resource

from employee.api.v1.employee.api import ns as employees
from employee.middleware.restplus import api

blueprint = Blueprint('api', __name__, url_prefix='/api/v1')
api.init_app(blueprint)

api.add_namespace(employees)
