from flask import jsonify, request
from flask_restplus import Resource
from marshmallow import fields

from employee.data_access.db_models import Employees
from employee.data_access.schema_definitions.employee_schema import EmployeeSchema
from employee.extensions import db, ma
from employee.middleware.restplus import api
from employee.serializers.employee_serializer import employee
from employee.utils.response_code import response_format
from employee.services.employee.service import (delete_employee,
                                                get_all_employee,
                                                get_employee, post_employee,
                                                update_employee, bulk_insert, bulk_update)

# from employee.utils.response_constants import RESPONSE_ERROR_MESSAGE
ns = api.namespace(
    'employees',
    description='Operations related to Employees')

@ns.route('/<int:employee_id>')
class EmpliyeeResource(Resource):
    """Single object resource
    """

    def get(self, employee_id):
        response = get_employee(employee_id)
        return response_format(response)

    @api.expect(employee)
    def put(self, employee_id):
        response = update_employee(employee_id, request.json)
        return response_format(response)

    def delete(self, employee_id):
        response = delete_employee(employee_id)
        if response:
            return response_format(response)


@ns.route('/')
class EmployeeList(Resource):
    """Creation and get_all
    """

    def get(self):
        response = get_all_employee()
        return response, 200

    @api.expect(employee)
    def post(self):
        response = post_employee(request.json)
        return response, 201


@ns.route('/bulk_insert')
class BulkInsert(Resource):
    """
    Bulk insert
    """
    def post(self):
        response = bulk_insert()
        return response, 201

@ns.route('/bulk_update')
class BulkUpdate(Resource):
    """
     Bulk update
    """
    def post(self):
        response = bulk_update()
        return response, 201
